# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-10 06:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Fitur_1', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='todo',
            name='title',
        ),
    ]
