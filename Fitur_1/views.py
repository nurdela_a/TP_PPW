from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo
from profiles.views import name

# Create your views here.
response = {}
def index(request):
    response['author'] = "Luthfi Dzaky, Nurdela Ardiansyah, Haikal Ravendy, M. Sulthan Rafi S." #TODO Implement yourname
    todo = Todo.objects.all()
    response['name']=name
    response['todo'] = todo
    html = 'fitur_1/fitur_1.html'
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')



