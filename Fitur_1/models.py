from django.db import models

class Todo(models.Model):
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)