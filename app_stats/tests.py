from django.test import TestCase
from django.test import Client
from django.urls import resolve
from Fitur_1.models import Todo
from .views import index  # Create your tests here.


class AppStatsTest(TestCase):
    def test_app_stats_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_app_stats_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_root_url_now_is_using_index_page_from_app_stats(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/dashboard/',301,200)

    def test_last_status_app_stats(self):
        laststatus="hehe"
        response_post = Client().post('/status/add_status', {'description': laststatus})
        status = Todo.objects.all().order_by('-created_date')[0]

        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(laststatus,html_response)