from django.shortcuts import render
from add_friend.models import Friends
from Fitur_1.models import Todo
from profiles.views import name

# Create your views here.
response = {}

def index(request):
    response['laststatus']=''
    response['created_date'] = ''
    response['name'] = name
    friend = Friends.objects.all().count();
    feeds = Todo.objects.all().count();
    if(feeds>0):
        status= Todo.objects.all().order_by('-created_date')[0]
        response['laststatus']=status.description
        response['created_date']=status.created_date
    response['author']= "Luthfi Dzaky, Nurdela Ardiansyah, Haikal Ravendy, M. Sulthan Rafi S."
    response['friend']=friend
    response['feeds']=feeds
    html = 'app_stats/app_stats.html'
    return render(request,html,response)
