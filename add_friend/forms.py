from django import forms

class Add_Friend(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan URL',
    }
    name_attrs = {
    	'type' : 'text',
        'class': 'form-control'
    }
    url_attrs = {
    	'type' : 'URL',
    	'class' : 'form-control'
    }

    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    URL = forms.URLField(label='URL',required=True, widget=forms.TextInput(attrs=url_attrs))