from django.db import models
from django.utils import timezone
import pytz

class Friends(models.Model):
	name = models.CharField(max_length=27)
	url = models.URLField()
	created_date = models.DateTimeField(auto_now_add=True)