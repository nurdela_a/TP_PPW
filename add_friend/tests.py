from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Friends
from .views import index

class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add_friend/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_using_index_func(self):
		found = resolve('/add_friend/')
		self.assertEqual(found.func, index)

	def test_model_can_add_friend(self):
		#Creating a new activity
		new_activity = Friends.objects.create(name='rafiganteng',url='rafi@rafiganteng.com')

		#Retrieving all available activity
		counting_all_friends= Friends.objects.all().count()
		self.assertEqual(counting_all_friends,1)

	def test_add_friend_post_success_and_render_the_result(self):
		test_name = 'namatester'
		test_url = 'http://rafiganteng.com'
		response_post = Client().post('/add_friend/add_friend', {'name':test_name, 'URL':test_url})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/add_friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(test_name, html_response)
		self.assertIn(test_url, html_response)

	def test_add_friend_post_error_and_render_the_result(self):
		test_name = 'namatester'
		test_url = 'http://rafiganteng.com'
		response_post = Client().post('/add_friend/add_friend', {'name':'', 'URL':''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/add_friend/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test_name, html_response)
		self.assertNotIn(test_url, html_response)