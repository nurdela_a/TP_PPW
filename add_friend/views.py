from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Friend
from .models import Friends

# Create your views here.

response = {}


def index(request):
    response['Add_Friend'] = Add_Friend
    response['author'] = "Luthfi Dzaky, Nurdela Ardiansyah, Haikal Ravendy, M. Sulthan Rafi S."
    # TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    my_friend = Friends.objects.all()
    response['my_friend'] = my_friend
    html = 'add_friend/add_friend.html'
    return render(request, html, response)


def adding_friend(request):
    form = Add_Friend(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['URL'] = request.POST['URL']
        my_friend = Friends(name=response['name'], url=response['URL'])
        my_friend.save()
        html = 'add_friend/add_friend.html'
        return HttpResponseRedirect('/add_friend/')
    else:
        return HttpResponseRedirect('/add_friend/')
