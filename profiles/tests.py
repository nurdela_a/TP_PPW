from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Profile, Expertise

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class ProfilePageUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_profile(self):
        # Creating a new profile
        new_profile = Profile.objects.create(name='Hepzibah Smith', birthday='01 Jan', gender='Female',
                                             description='Antique expert. Experience as marketer for 10 years',
                                             email='hello@smith.com')

        # Counting all available profiles
        counting_all_profiles = Profile.objects.all().count()
        self.assertEqual(counting_all_profiles, 1)

    def test_model_can_create_new_expertise(self):
        # Creating a new expertise
        expertise = Expertise.objects.create(expertise='Coding')
        # Counting all available expertise
        counting_all_expertise = Expertise.objects.all().count()
        self.assertEqual(counting_all_expertise, 1)

    def test_tostring_function_in_expertise(self):
        dummy_expertise = "Coding"
        expertise = Expertise.objects.create(expertise=dummy_expertise)

        self.assertEqual(dummy_expertise, expertise.__str__())

    def test_profile_page_using_profile_page_html(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profiles/profiles.html')
