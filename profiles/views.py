from django.shortcuts import render
from .models import Profile, Expertise


# Create your views here.
response = {}
name = 'Hepzibah Smith'
def index(request):
    # Create a new profile
    count_all_profiles = Profile.objects.count()

    # if count_all_profiles == 0:
    profile = Profile.objects.create(
        profile_pic='https://pre00.deviantart.net/19f7/th/pre/f/2014/097/2/0/nyanko_sensei__dinner_s_ready__by_oboeteru-d7djql9.png',
        name='Hepzibah Smith', birthday='12 Oct', gender='Male',
        description='A stressed overachiever.', email='spence@hastings.com')
    profile.save()

    e1 = Expertise.objects.create(expertise='Fun Coding')
    e1.save()
    e2 = Expertise.objects.create(expertise='Modeling')
    e2.save()
    e3 = Expertise.objects.create(expertise='Taking Tests')
    e3.save()

    profile.expertise.add(e1)
    profile.expertise.add(e2)
    profile.expertise.add(e3)
    # else:
    #     profile = Profile.objects.last()

    # Inserting profile aspects into response
    response['profile_pic'] = profile.profile_pic
    response['name'] = profile.name
    response['birthday'] = profile.birthday
    response['gender'] = profile.gender
    response['expertise'] = profile.expertise.all()
    response['description'] = profile.description
    response['email'] = profile.email

    # Specifying html

    response['author'] = "Luthfi Dzaky, Nurdela Ardiansyah, Haikal Ravendy, M. Sulthan Rafi S."
    # response['name'] = name
    html = 'profiles/profiles.html'
    # response['about_me'] = about_me

    return render(request, html, response)
